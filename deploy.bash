#!/bin/bash

IDPTOOLS_HOME=~/code/idp-tools/
echo "at $(date '+%Y.%m.%d_%Hh%Mm%Ss')"
git_header="$(git -C  ${IDPTOOLS_HOME} log --pretty=format:'%H' | head -1)"
sed "s|GIT_HASH_VERSION_OF_IDP_TOOLS=.*|GIT_HASH_VERSION_OF_IDP_TOOLS='${git_header}'|" ${IDPTOOLS_HOME}/idp-tools > ${IDPTOOLS_HOME}/idp-tools.to_deploy
chmod +x ${IDPTOOLS_HOME}/idp-tools.to_deploy
for h in luniidplab{1,2} luniidp{tst,prd}{5,6}
do
    echo $h
    scp ${IDPTOOLS_HOME}/idp-tools.to_deploy  root@${h}:/usr/local/bin/idp-tools
done
